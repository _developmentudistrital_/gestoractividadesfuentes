export class Integrante {
    public Id: number;
    public Codigo: string;
    public Nombre: string;
    public Apellido: string;
    public Cargo: string;
    public IdEquipo: number;
    constructor() {

    }
}
