import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { GestorActividadComponent } from './pages/gestor-actividad/gestor-actividad.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { IntegrantesComponent } from './components/integrantes/integrantes.component';
import { ActividadesComponent } from './components/actividades/actividades.component';
import { ActividadService } from './services/actividad.service';
import { EquipoService } from './services/equipo.service';
import { IntegranteService } from './services/integrante.service';
import { ProyectoService } from './services/proyecto.service';

@NgModule({
  declarations: [
    AppComponent,
    GestorActividadComponent,
    NavbarComponent,
    IntegrantesComponent,
    ActividadesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ActividadService,
    EquipoService,
    IntegranteService,
    ProyectoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
