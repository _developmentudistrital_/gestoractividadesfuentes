import { Component, OnInit } from '@angular/core';
import { Actividad } from 'src/app/models/actividad.model';
import { Proyecto } from 'src/app/models/proyecto.model';
import { Equipo } from 'src/app/models/equipo.model';
import { ActividadService } from 'src/app/services/actividad.service';
import { EquipoService } from 'src/app/services/equipo.service';
import { IntegranteService } from 'src/app/services/integrante.service';
import { ProyectoService } from 'src/app/services/proyecto.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Integrante } from 'src/app/models/integrante.model';



@Component({
  selector: 'app-gestor-actividad',
  templateUrl: './gestor-actividad.component.html',
  styleUrls: ['./gestor-actividad.component.css']
})
export class GestorActividadComponent implements OnInit {


  Integrantes: Integrante[] = [];
  Actividades: Actividad[] = [];
  Proyectos: Proyecto[] = [];
  Equipos: Equipo[] = [];
  IdProyecto:number;
  IdEquipo:number;
  FormActividad: FormGroup;
  Prueba:number;

  constructor(
    public serviceActividad: ActividadService,
    public serviceEquipo: EquipoService,
    public serviceIntegrante: IntegranteService,
    public serviceProyecto: ProyectoService
  ) {
    this.initForm();
  }
  private initForm() {
    this.FormActividad = new FormGroup({
      idProyecto: new FormControl(''),
      idEquipo: new FormControl(''),
      Actividad: new FormGroup({
        codigo: new FormControl(''),
        actividad: new FormControl(''),
        tiempo: new FormControl(''),
        entregable: new FormControl(''),
      })
    });
  }

  ngOnInit() {
    //this.Proyectos= this.serviceProyecto
    this.serviceProyecto.getProyectos().subscribe(response => {
      this.Proyectos = response;
    });

    this.serviceEquipo.getEquipos().subscribe(response => {
      this.Equipos = response;
    });
  }

  public changeEquipo(idEquipo: string) {
    let sonar;
    var sonarq;
    var n = NaN;

    if(n===NaN){
      console.log("no number");
    }
    if (idEquipo != "") {
      this.consultarIntegrantesxEquipo(parseInt(idEquipo));
      this.IdEquipo=parseInt(idEquipo);
      this.consultarActividades(this.IdProyecto, this.IdEquipo);
    } else {
      this.Integrantes = [];
      this.Actividades = [];
    }

  }
  public changeProyecto(idProyecto: string) {
    if (idProyecto != "") {
      this.IdProyecto=parseInt(idProyecto);
      //this.consultarIntegrantesxEquipo(parseInt(idEquipo));
      this.consultarActividades(this.IdProyecto, this.IdEquipo);
    } else {
      this.Actividades = [];
    }

  }
  public consultarIntegrantesxEquipo(idEquipo: number) {
    this.serviceIntegrante.getIntegrantes(idEquipo).subscribe(response => {
      this.Integrantes = response;
    });
  }

  public guardarActividad() {
    let actividad: Actividad = new Actividad();
    let codigo: number;
    let descripcion: string;
    let tiempo: number;
    let entregable: string;
    let idEquipo: number;
    let idProyecto: number;

    idEquipo = parseInt(this.FormActividad.get("idEquipo").value);
    idProyecto = parseInt(this.FormActividad.get("idProyecto").value);
    codigo = parseInt(this.FormActividad.get("Actividad.codigo").value);
    descripcion = this.FormActividad.get("Actividad.actividad").value;
    tiempo = parseInt(this.FormActividad.get("Actividad.tiempo").value);
    entregable = this.FormActividad.get("Actividad.entregable").value;
    // if ((!isNaN(codigo) && !isNaN(tiempo) && !isNaN(idEquipo) && !isNaN(idProyecto))  ) {
    //   console.error('Datos numericos no validos');
    // } 
    
    actividad.Codigo = codigo;
    actividad.Descripcion = descripcion;
    actividad.Tiempo = tiempo;
    actividad.Entregable = entregable;
    actividad.IdEquipo = idEquipo;
    actividad.IdProyecto = idProyecto;

    this.serviceActividad.adicionarActividad(actividad);
    this.limpiarActividad();
    this.consultarActividades(this.IdProyecto, this.IdEquipo);

    if ((isNaN(codigo) || isNaN(tiempo) || isNaN(idEquipo) || isNaN(idProyecto))) {
      console.error('Datos numericos no validos');
    }

  }
  public limpiarActividad() {
    this.FormActividad.get('Actividad').reset();
  }
  public consultarActividades(idProyecto: number, idEquipo: number) {
    this.Actividades = this.serviceActividad.getActividades(idProyecto, idEquipo);
  }

  public eliminarActividad(actividad:Actividad){
    this.serviceActividad.eliminarActividad(actividad);
    this.consultarActividades(this.IdProyecto, this.IdEquipo);
  }

}
