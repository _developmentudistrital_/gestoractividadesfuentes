import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestorActividadComponent } from './gestor-actividad.component';

describe('GestorActividadComponent', () => {
  let component: GestorActividadComponent;
  let fixture: ComponentFixture<GestorActividadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestorActividadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestorActividadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
