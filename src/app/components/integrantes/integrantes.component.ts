import { Component, OnInit, Input } from '@angular/core';
import { Integrante } from 'src/app/models/integrante.model';

@Component({
  selector: 'app-integrantes',
  templateUrl: './integrantes.component.html',
  styleUrls: ['./integrantes.component.css']
})
export class IntegrantesComponent implements OnInit {

  @Input() Integrantes:Integrante[];
  constructor() { }

  ngOnInit() {
  }

}
