import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Actividad } from 'src/app/models/actividad.model';

@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.component.html',
  styleUrls: ['./actividades.component.css']
})
export class ActividadesComponent implements OnInit {

  @Input() Actividades: Actividad[];
  @Output() EliminarActividad = new EventEmitter<Actividad>();

  constructor() { }

  ngOnInit() {
  }

  public eliminarActividad(actividad: Actividad) {
    this.EliminarActividad.emit(actividad);
  }

}
