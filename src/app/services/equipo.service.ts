import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Equipo } from 'src/app/models/equipo.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EquipoService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getEquipos(): Observable<Equipo[]> {
    return this.httpClient.get('../../assets/json/equipos.json')
      .pipe(map((data: Equipo[]) => {
        return data;
      }));
  }
}
