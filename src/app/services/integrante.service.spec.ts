import { TestBed } from '@angular/core/testing';

import { IntegranteService } from './integrante.service';

describe('IntegranteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IntegranteService = TestBed.get(IntegranteService);
    expect(service).toBeTruthy();
  });
});
