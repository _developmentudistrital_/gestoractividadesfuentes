import { Injectable } from '@angular/core';
import { Actividad } from '../models/actividad.model';

@Injectable({
  providedIn: 'root'
})
export class ActividadService {

  Actividades: Actividad[] = [];
  constructor() { }

  public adicionarActividad(actividad: Actividad) {
    this.Actividades.push(actividad);
  }
  public getActividades(idProyecto: number, idEquipo: number): Actividad[] {
    return this.Actividades.filter(actividad => actividad.IdProyecto == idProyecto && actividad.IdEquipo == idEquipo);
  }
  public eliminarActividad(actividad: Actividad) {
    this.Actividades.splice(this.Actividades.indexOf(actividad), 1);
  }
  public modificarActividad(actividad: Actividad) {
    let index = this.Actividades.indexOf(actividad);
    this.Actividades[index] = actividad;
  }

}
