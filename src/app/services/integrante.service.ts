import { Injectable } from '@angular/core';
import { Integrante } from 'src/app/models/integrante.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IntegranteService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getIntegrantes(idEquipo: number): Observable<Integrante[]> {
    return this.httpClient.get('../../assets/json/integrantes.json')
      .pipe(map((data: Integrante[]) => {
        return data.filter(integrante => integrante.IdEquipo == idEquipo);
      }));
  }
}
