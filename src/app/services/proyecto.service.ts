import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Proyecto } from 'src/app/models/proyecto.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProyectoService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getProyectos(): Observable<Proyecto[]> {
    return this.httpClient.get('../../assets/json/proyectos.json')
      .pipe(map((data: Proyecto[]) => {
        return data;
      }));
  }
}
